import getdist.plots as gplot
import os

analysis_settings = {'max_corr_2D': u'0.99', 'boundary_correction_order': u'1', 'converge_test_limit': u'0.95', 'smooth_scale_2D': u'0.3', 'credible_interval_threshold': u'0.05', 'contours': u'0.68 0.95 0.99', 'fine_bins_2D': u'400', 'num_bins': u'100', 'mult_bias_correction_order': u'1', 'fine_bins': u'3072', 'num_bins_2D': u'40', 'max_scatter_points': u'2000', 'range_ND_contour': u'-1', 'range_confidence': u'0.01', 'smooth_scale_1D': u'-1', 'ignore_rows': u'0'}
param_3d = None
params=[u'z', u'sys_err0', u'log(age/yr)[0,1]', u'log(tau/yr)[0,1]', u'log(Z/Zsun)[0,1]', u'A_v/mag[0,1]']
roots = ['10csp200_bc2003_lr_BaSeL_chab000_hyperz5_sample_par']

g=gplot.getSubplotPlotter(chain_dir=r'./test/ULTRAVISTA0/ULTRAVISTA114558',analysis_settings=analysis_settings)
g.settings.colormap = "jet"
g.settings.lab_fontsize=12
g.settings.axes_fontsize
g.triangle_plot(roots, params, plot_3d_with_param=param_3d, filled=False, shaded=True)
g.export("pdftreex.eps")

g=gplot.getSubplotPlotter(chain_dir=r'./test/ULTRAVISTA0/ULTRAVISTA99938',analysis_settings=analysis_settings)
g.settings.colormap = "jet"
g.settings.lab_fontsize=12
g.triangle_plot(roots, params, plot_3d_with_param=param_3d, filled=False, shaded=True)
g.export("pdftreey.eps")
