TALKS
======================================
- `Cosmic dawn of galaxy formation: linking observations and theory with new-generation spectral models <http://www.iap.fr/col2016>`_

  `Bayesian interpretation of the spectral energy distributions of galaxies with BayeSED <https://bitbucket.org/hanyk/bayesed/src/V2.0/docs/talks/2016IAP_hanyk.pdf>`_

- `IAU Symposium 341: PanModel2018: Challenges in Panchromatic Galaxy Modelling with Next Generation Facilities <https://panmodel2018.sciencesconf.org>`_

  `Bayesian discrimination of the panchromatic spectral energy distribution modelings of galaxies <https://bitbucket.org/hanyk/bayesed/src/V2.0/docs/talks/Day3_am_Han_Yunkun.pdf>`_

- `The Art of Measuring Galaxy Physical Properties <https://zenodo.org/communities/app-iasfmi-2019>`_

  `Modelling and interpreting the multi-wavelength spectral energy distributions of galaxies with machine learning and Bayesian inference <https://zenodo.org/record/3553688>`_
