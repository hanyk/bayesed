USAGE
======================================
.. highlight:: none

::

    ./bayesed (openmpi v1.8.8 is required for mpirun) [OPTIONS] -i inputfile

    OPTIONS:

    -a, --ann ARG1[,ARGn]               Select  ann model by name
                                        e.g. -a id,bc03_pr_exp_ch_calzetti,iscalable

    -ak, --aknn ARG1[,ARGn]             Select aknn model by name
                                        e.g. -ak id,bc03_pr_exp_ch_calzetti,iscalable,k,f_run,eps,iRad

    -bb, --blackbody ARG1[,ARGn]        black body spectrum
                                        e.g. --blackbody id,bb,iscalable,w_min,w_max,Nw

    -gb, --greybody ARG1[,ARGn]         grey body spectrum
                                        e.g. --greybody id,gb,iscalable,ithick,w_min,w_max,Nw

    -h, -help, --help, --usage          Display usage instructions

    -i, --input ARG1[,ARGn]             Input file containing observed photometric SEDs with given unit(0 for flux in uJy, 1 for AB magnitude)
                                        e.g. -i 0,observation/ULTRAVISTA/ULTRAVISTA0.txt

    -k, --knn ARG1[,ARGn]               Select knn model by name
                                        e.g. -k id,bc03_pr_exp_ch_calzetti,iscalable,k,f_run

    -L, --luminosity ARG1[,ARGn]        compute luminosity between w_min and w_max in rest-frame
                                        e.g. --luminosity w_min,wmax

    -pw, --powerlaw ARG1[,ARGn]         power law spectrum
                                        e.g. --powerlaw id,pw,iscalable,w_min,w_max,Nw

    -s, --sampling ARG1[,ARGn]          IS,mmodal,ceff,nlive,efr,tol,updInt,Ztol,seed,fb,resume,outfile,logZero,maxiter,acpt_min for MultiNest.
                                        e.g. --sampling 1,0,0,400,0.3,0.1,1000,-1e90,1,0,0,0,-1e90,100000,0.01 (default)

    -sfh, --sfh ARG1[,ARGn]             Select a SFH for the csp model
                                        e.g. -sfh id,itype_sfh,itruncated,itype_ceh

    -ssp, --ssp ARG1[,ARGn]             Select a ssp model for the csp model
                                        e.g. -ssp id,ynII,iscalable,k,f_run,eps,iRad,iT0,iT1,iT2

    -t, --template ARG1[,ARGn]          Use template SED with the given name
                                        e.g. -t id,M82,iscalable

    --check                             Print all inputs and their category.

    --cl ARG1[,ARGn]                    output estimates at confidence levels e.g. --cl 0.68,0.95 (default)

    --cosmology ARG1[,ARGn]             e.g. --cosmology 70,0.7,0.3 (default)

    --ebv ARG1[,ARGn]                   set the global value of E(B-V) of MW for all objects
                                        e.g. --ebv 0

    --export ARG                        Exports all options including the default values of unset ones.
                                        e.g. --export out.txt

    --ext ARG1[,ARGn]                   select extinction curve
                                        e.g. --ext id,ext_law[0-7]
                                        0:Starburst(Calzetti+2000,FAST)
                                        1:Milky Way (Cardelli+1989,FAST)
                                        2:Star-forming(Noll+2009,FAST)
                                        3:MW(Allen+76,hyperz)
                                        4:MW(Fitzpatrick+86,hyperz)
                                        5:LMC(Fitzpatrick+86,hyperz)
                                        6:SMC(Fitzpatrick+86,hyperz)
                                        7:SB(Calzetti2000,hyperz)

    --filters ARG                       Set the file containing the definition of filters
                                        e.g. --filters filter/filters.txt

    --filters_selected ARG              Set all used filters in the observation and select those needed
                                        e.g. --filters_selected filter/filters_selected.txt

    --flux_max ARG                      maximum allowed observational flux in microJy
                                        e.g. --flux_max 1e99 (default)

    --flux_min ARG                      minimum allowed observational flux in microJy
                                        e.g. --flux_min 0 (default)

    --gsl_integration_qag ARG1[,ARGn]   set epsabs,epsrel,limit for gsl_integration_qag
                                        e.g. --gsl_integration_qag 0,0.01,500 (default)

    --import ARG1[,ARGn]                Import one or more files that contain command line options and use # as comment char.
                                        e.g. --import in.txt

    --IGM ARG1[,ARGn]                   --IGM [0-5]
                                        Select the model for intergalactic medium attenuation
                                        0:None
                                        1:Madau (1995) model (default)
                                        2:Meiksin (2006) model

    --load_priors ARG                   load priors from .hist files for all objects
                                        e.g. --load_priors priors_root

    --nzbin ARG                         Number of bins used to sample the prior of redshift.
                                        e.g. --nzbin 20 (default)

    --NNF ARG1[,ARGn]                   The MAXITER and toler used in nonnegative quadratic programming.
                                        e.g. --NNF 10000,0 (default)

    --NfilterPoints ARG                 e.g. --NfilterPoints 30 (default)

    --outdir ARG                        output dir for all results
                                        e.g. --ourdir result/ (default)

    --output_PCs ARG                    output the amplitude of first N PCs
                                        e.g. --output_PCs 0 (default)

    --output_model_absolute_magnitude   output model absolute magnitude of best fit

    --output_model_apparent_magnitude   output model apparent magnitude of best fit

    --output_model_flux                 output model flux of best fit

    --output_pos_obs                    output posterior estimation of observables

    --rename ARG                        rename the model

    --save_bestfit                      Save the best fitting result

    --save_pos_sfh ARG                  Save the posterior distribution of model SFH with given Ngrid
                                        e.g. --save_pos_sfh 100

    --save_pos_spec                     Save the posterior distribution of model spectra (Warning,require memory of size nSamples*Nwavelengths!)

    --save_priors                       save priors as .hist files for all objects

    --save_sample_obs                   save posteriori sample of observables

    --save_sample_par                   Save the posterior sample of parameters

    --save_sample_spec                  Save the posterior sample of model spectra

    --save_summary                      Save the summary file

    --select_sample ARG1[,ARGn]         select a subsample with given conditions
                                        e.g. --select_sample Nvalid_photo_min,Nvalid_spectra_min,SNR_min

    --sys_err_model ARG1[,ARGn]         fractional systematic error of model
                                        e.g. --sys_err_model 0,0 (default)

    --sys_err_obs ARG1[,ARGn]           fractional systematic error of obs
                                        e.g. --sys_err_obs 0,0 (default)

    --test_priors                       test priors by setting the loglike for observational data to be zero

    --w_max ARG                         maximum allowed observational wavelength in micron

    --w_min ARG                         minimum allowed observational wavelength in micron

    --zrange ARG1[,ARGn]                set the global value of z_min and z_max used for the fitting of all objects
                                        e.g. --zrange 0,6

