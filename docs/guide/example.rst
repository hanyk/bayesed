EXAMPLES
======================================

run_test
--------

Set the run time parameters for MultiNest sampling: 

.. code:: bash

    --sampling 1,0,0,200,0.8,0.1,1000,-1e90,1,2,0,0,-1e90,100000,0.01

Set the input file:

.. code:: bash

 -i 0,observation/ULTRAVISTA/test.txt

.. literalinclude:: ../../observation/ULTRAVISTA/test.txt

Set the files for the definition and selection of filters:

.. code:: bash

     --filters observation/ULTRAVISTA/filters.txt --filters_selected observation/ULTRAVISTA/filters_selected.txt

:download:`filters.txt <../../observation/ULTRAVISTA/filters.txt>`
:download:`filters_selected.txt <../../observation/ULTRAVISTA/filters_selected.txt>`

.. literalinclude:: ../../observation/ULTRAVISTA/filters_selected.txt

Set the SED model (SSP, star-formation histroy and dust extinction law):

.. code:: bash

     --ssp 0,bc2003_lr_BaSeL_chab,1,3,1,0,0,0,0,0 --sfh 0,2,0,0 --ext 0,7

Set output directory of all results:

.. code:: bash

    --outdir test

Set the systematic error in the data as a free parameter:

.. code:: bash

    --sys_err_obs 0,1

Set the allowed redshift range which will override that set in the inputfile:

.. code:: bash

     --zrange 0,6

Save the best-fitting results and the posteriori PDFs of model spectra, SFH, and parameters:

.. code:: bash

    --save_bestfit --save_pos_spec --save_pos_sfh 100 --save_sample_par

Full command to run:

.. code:: bash

    time ./bayesed --sampling 1,0,0,200,0.8,0.1,1000,-1e90,1,2,0,0,-1e90,100000,0.01 -i 0,observation/ULTRAVISTA/test.txt --filters observation/ULTRAVISTA/filters.txt --filters_selected observation/ULTRAVISTA/filters_selected.txt --ssp 0,bc2003_lr_BaSeL_chab,1,3,1,0,0,0,0,0 --sfh 0,2,0,0 --ext 0,7 --outdir test --sys_err_obs 0,1 --zrange 0,6 --save_bestfit --save_pos_spec --save_pos_sfh 100 --save_sample_par

The results of SED fitting for the PEG ULTRAVISTA114558 (left) and the SFG ULTRAVISTA99938 (right). Except for the best-fit SED, the median, 68% and 95% credible region obtained from the posterior PDFs of the model SEDs are also shown. The GALEX FUV and NUV, Spitzer IRAC 3.6 and 4.5um data have been labeled in the figure.

.. figure:: ../images/plot_fit.jpg


:download:`plot_fit.gpi <plot_fit.gpi>`

.. literalinclude:: plot_fit.gpi
   :language: bash


The posterior PDF for the SFH of the PEG ULTRAVISTA114558 (left) and the SFG ULTRAVISTA99938 (right). Only the median, 68% and 95% credible region obtained from the posterior PDF of the SFH for the two galaxies are shown.

.. figure:: ../images/plot_sfh.jpg

:download:`plot_sfh.gpi <plot_sfh.gpi>`

.. literalinclude:: plot_sfh.gpi
   :language: bash

.. figure:: ../images/pdftreex.jpg

    The 1 and 2-D posterior PDFs of free parameters for the PEG ULTRAVISTA114558. They represent our state of knowledge about them. The presence of multiple peaks and/or strong correlations in the 2D PDFs indicate the degeneracies between the free parameters of the SED model.

.. figure:: ../images/pdftreey.jpg

    The 1 and 2-D posterior PDFs of free parameters for the SFG ULTRAVISTA99938. They represent our state of knowledge about them. The presence of multiple peaks and/or strong correlations in the 2D PDFs indicate the degeneracies between the free parameters of the SED model.

:download:`pdftree.py <pdftree.py>`

.. literalinclude:: pdftree.py

run_HH2019
----------

`openmpi v1.8.8 <https://download.open-mpi.org/release/open-mpi/v1.8/openmpi-1.8.8.tar.bz2>`_ is required for mpirun

.. code:: bash

    ./run_HH2019 160

.. literalinclude:: ../../run_HH2019
   :language: bash
