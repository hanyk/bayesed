SED model emulation with machine learning
=========================================
As an example, we show how the SED library of `CLUMPY AGN dust torus emission model <https://www.pa.uky.edu/clumpy>`_ can be imported into BayeSED and then used in the Bayesian analysis of photometric measurements of NGC1068 from `GALSEDATLAS <https://archive.stsci.edu/hlsp/galsedatlas>`_. 

Preparation of SED library:

.. code:: bash

    wget -P models https://www.clumpy.org/downloads/clumpy_models_201410_tvavg.hdf5
    ./models/clumpy/convert_clumpy.py tor

Principal Component Analysis (PCA):

.. code:: bash

    ./bin/mac/sed2pca clumpy201410tor 1 0.1 1e-6

K-Nearest Neighbors (KNN) algorithm:

.. code:: bash

    ./bin/train_knn clumpy201410tor 1 1 1e-6 mac
    ./bin/train_knn clumpy201410tor_derived 1 0 1e-6 mac
    time ./bin/mac/bayesed --sampling 1,0,0,200,0.8,0.1,1000,-1e90,1,1,0,0,-1e90,100000,0.01 -i 1,observation/hlsps/test.txt --filters observation/hlsps/filters.txt --filters_selected observation/hlsps/filters_selected_gal.txt --ssp 0,bc2003_lr_BaSeL_kroup,1,3,1,0,0,0,0,0 --sfh 0,2,0,0 --ext 0,7 -ak 1,clumpy201410tor,1,1,1,0,0 --sys_err_obs 0,1 --outdir galsedatlas0 --save_bestfit

Artificial Neural Network (ANN):

.. code:: bash

    ./bin/train_ann clumpy201410tor 1 1 1e-6 mac
    ./bin/train_ann clumpy201410tor_derived 1 0 1e-6 mac
    time ./bin/mac/bayesed --sampling 1,0,0,200,0.8,0.1,1000,-1e90,1,1,0,0,-1e90,100000,0.01 -i 1,observation/hlsps/test.txt --filters observation/hlsps/filters.txt --filters_selected observation/hlsps/filters_selected_gal.txt --ssp 0,bc2003_lr_BaSeL_kroup,1,3,1,0,0,0,0,0 --sfh 0,2,0,0 --ext 0,7 -a 1,clumpy201410tor,1  --sys_err_obs 0,1 --outdir galsedatlas0 --save_bestfit

Plot of best-fit results

.. code:: bash

    gnuplot observation/hlsps/plot_bestfit_NGC1068.gpi

:download:`plot_bestfit_NGC1068.gpi <../../observation/hlsps/plot_bestfit_NGC1068.gpi>`

.. literalinclude::  ../../observation/hlsps/plot_bestfit_NGC1068.gpi
   :language: bash

.. figure:: ../images/NGC1068.jpg
