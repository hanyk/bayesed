**BayeSED**: **Baye**\ sian interpretation of the **SED**\ s of galaxies
==============
.. image:: https://img.shields.io/badge/bitbucket.org-hanyk%2Fbayesed-blue.svg
	:target: https://bitbucket.org/hanyk/bayesed
.. image:: https://readthedocs.org/projects/bayesed/badge/?version=v2.0
	:target: http://bayesed.readthedocs.io/en/v2.0/?badge=v2.0
.. image:: http://img.shields.io/badge/license-MIT-blue.svg?style=flat
	:target: https://bayesed.readthedocs.io/en/v2.0/LICENSE.html

``BayeSED`` is a general tool for the full Bayesian interpretation of the spectral energy distributions (SEDs) of galaxies.
Given the multi-band photometries of galaxies, it can be used for the Bayesian parameter estimation by posteriori probability distributions (PDFs) and the Bayesian SED model comparison by Bayesian evidence.
Except for the build-in SED models (stellar population synthesis models, blackbody, greybody and powerlaw), other SED models can be emulated with machine learning techniques.
The linear combination of all selected SED model components will then be used for the full Bayesian interpretation of the observational SEDs of galaxies.

**Documentation**
-----------------  
Read the docs `here <http://bayesed.readthedocs.io/>`_.

**Issues**
----------
If you find any problems with ``BayeSED``, please create a new issue `here <https://bitbucket.org/hanyk/bayesed/issues/new>`_, or e-mail your questions to hanyk@ynao.ac.cn.

**Citation**
------------
If you use ``BayeSED`` in your research or have been inspired by it, we kindly remind you to cite our papers (`Han, Y., & Han, Z. 2012, ApJ, 749, 123 <http://adsabs.harvard.edu/abs/2012ApJ...749..123H>`_; `Han, Y., & Han, Z. 2014, ApJS, 215, 2 <http://adsabs.harvard.edu/abs/2014ApJS..215....2H>`_; `Han, Y., & Han, Z. 2019, ApJS, 240, 3 <http://adsabs.harvard.edu/abs/2019ApJS..240....3H>`_).

**License**
-----------
Copyright (C) 2014-2019 Yunkun Han(hanyk@ynao.ac.cn).

``BayeSED`` is made freely available under  `the MIT license <https://bayesed.readthedocs.io/en/v2.0/LICENSE.html>`_.
