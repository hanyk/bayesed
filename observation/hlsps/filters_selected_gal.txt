#iused    iselected id        name      # type beta  information
1         1         7         FUV       # 1    0     GALEX_FUV.filter
1         1         37        UVW2      # 1    0     swift.uvw2.dat
1         1         35        UVM2      # 1    0     swift.uvm2.dat
1         1         8         NUV       # 1    0     GALEX_NUV.filter
1         1         36        UVW1      # 1    0     swift.uvw1.dat
1         1         34        U         # 1    0     swift.u.dat
1         1         39        u         # 1    0     u_sdss.dat
1         1         30        g         # 1    0     g_sdss.dat
1         1         38        V         # 1    0     swift.v.dat
1         1         32        r         # 1    0     r_sdss.dat
1         1         31        i         # 1    0     i_sdss.dat
1         1         40        z         # 1    0     z_sdss.dat
1         1         5         J         # 1    0     2MASS_J.filter
1         1         4         H         # 1    0     2MASS_H.filter
1         1         6         Ks        # 1    0     2MASS_K.filter
1         1         20        W1        # 1    0     RSR-W1.txt
1         1         0         [3.6]     # 1    1     080924ch1trans_full.txt
1         1         1         [4.5]     # 1    1     080924ch2trans_full.txt
1         1         21        W2        # 1    0     RSR-W2.txt
1         1         2         [5.8]     # 1    1     080924ch3trans_full.txt
1         1         3         [8.0]     # 1    1     080924ch4trans_full.txt
1         1         22        W3        # 1    0     RSR-W3.txt
1         1         41        IRS_PB    # 1    0     IRS blue peak-up (16um) [added April 2009]
1         1         23        W4        # 1    0     RSR-W4.txt
1         1         42        IRS_PR    # 1    0     IRS red peak-up [added Dec 2012]
1         1         43        [24]      # 1    2     MIPS 24 micron filter
