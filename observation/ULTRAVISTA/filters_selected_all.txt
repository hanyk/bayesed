#iused iselected id name zp_offset_a zp_offset_b information
0 0 0 F0 1 0 # 1 0   133 hst/ACS_update_sep07/wfc_f435w_t77.dat  obs_before_7-4-06+rebin-5A  lambda_c= 4.3179e+03  AB-Vega=-0.104
0 0 1 F1 1 0 # 1 0   121 hst/ACS_update_sep07/wfc_f475w_t77.dat  obs_before_7-4-06+rebin-5A  lambda_c= 4.7453e+03  AB-Vega=-0.101
0 0 2 F2 1 0 # 1 0   118 hst/ACS_update_sep07/wfc_f555w_t77.dat  obs_before_7-4-06+rebin-5A  lambda_c= 5.3601e+03  AB-Vega=-0.009
0 0 3 F3 1 0 # 1 0   173 hst/ACS_update_sep07/wfc_f606w_t77.dat  obs_before_7-4-06+rebin-5A  lambda_c= 5.9194e+03  AB-Vega= 0.082
0 0 4 F4 1 0 # 1 0    86 hst/ACS_update_sep07/wfc_f775w_t77.dat  obs_before_7-4-06+rebin-5A  lambda_c= 7.6933e+03  AB-Vega= 0.385
0 0 5 F5 1 0 # 1 0   117 hst/ACS_update_sep07/wfc_f814w_t77.dat  obs_before_7-4-06+rebin-5A  lambda_c= 8.0599e+03  AB-Vega= 0.419
0 0 6 F6 1 0 # 1 0   102 hst/ACS_update_sep07/wfc_f850lp_t77.dat  obs_before_7-4-06+rebin-5A  lambda_c= 9.0364e+03  AB-Vega= 0.519
0 0 7 F7 1 0 # 1 0   193 hst/nicmos_f110w.dat  synphot-calcband  lambda_c= 1.1234e+04  AB-Vega= 0.725
0 0 8 F8 1 0 # 1 0   110 hst/nicmos_f160w.dat  synphot-calcband  lambda_c= 1.6037e+04  AB-Vega= 1.306
0 0 9 F9 1 0 # 1 0  1409 hst/wfpc2_f300w.dat  synphot-calcband  lambda_c= 2.9928e+03  AB-Vega= 1.350
0 0 10 F10 1 0 # 1 0   469 hst/wfpc2_f336w.dat  synphot-calcband  lambda_c= 3.3595e+03  AB-Vega= 1.182
0 0 11 F11 1 0 # 1 0   183 hst/wfpc2_f450w.dat  synphot-calcband  lambda_c= 4.5573e+03  AB-Vega=-0.086
0 0 12 F12 1 0 # 1 0   208 hst/wfpc2_f555w.dat  synphot-calcband  lambda_c= 5.4429e+03  AB-Vega=-0.004
0 0 13 F13 1 0 # 1 0   142 hst/wfpc2_f606w.dat  synphot-calcband  lambda_c= 6.0013e+03  AB-Vega= 0.098
0 0 14 F14 1 0 # 1 0   149 hst/wfpc2_f702w.dat  synphot-calcband  lambda_c= 6.9171e+03  AB-Vega= 0.260
0 0 15 F15 1 0 # 1 0   125 hst/wfpc2_f814w.dat  synphot-calcband  lambda_c= 7.9960e+03  AB-Vega= 0.412
0 0 16 F16 1 0 # 1 0   102 hst/wfpc2_f850lp.dat  synphot-calcband  lambda_c= 9.1140e+03  AB-Vega= 0.516
0 0 17 F17 1 0 # 1 1    94 IRAC/irac_tr1_2004-08-09.dat  3.6micron  lambda_c= 3.5569e+04  AB-Vega= 2.781
0 0 18 F18 1 0 # 1 1    86 IRAC/irac_tr2_2004-08-09.dat  4.5micron  lambda_c= 4.5020e+04  AB-Vega= 3.254
0 0 19 F19 1 0 # 1 1    95 IRAC/irac_tr3_2004-08-09.dat  5.8micron  lambda_c= 5.7450e+04  AB-Vega= 3.747
0 0 20 F20 1 0 # 1 1   158 IRAC/irac_tr4_2004-08-09.dat  8.0micron  lambda_c= 7.9158e+04  AB-Vega= 4.387
0 0 21 F21 1 0 # 1 0   178 DEEP2-VVDS/mouldB_cfh7403.dat   +atm  lambda_c= 4.3122e+03  AB-Vega=-0.107
0 0 22 F22 1 0 # 1 0   237 DEEP2-VVDS/mouldV_cfh7503.dat   +atm  lambda_c= 5.3564e+03  AB-Vega=-0.011
0 0 23 F23 1 0 # 1 0    86 DEEP2-VVDS/mouldR_cfh7603.dat   +atm  lambda_c= 6.5528e+03  AB-Vega= 0.206
0 0 24 F24 1 0 # 1 0   116 DEEP2-VVDS/mouldI_cfh7802.dat   +atm  lambda_c= 8.1551e+03  AB-Vega= 0.435
0 0 25 F25 1 0 # 1 0    38 KPNO/IRIMJ  HDF-N+atm  lambda_c= 1.2289e+04  AB-Vega= 0.879
0 0 26 F26 1 0 # 1 0    43 KPNO/IRIMH  HDF-N+atm  lambda_c= 1.6444e+04  AB-Vega= 1.359
0 0 27 F27 1 0 # 1 0    62 KPNO/IRIMK  HDF-N+atm  lambda_c= 2.2124e+04  AB-Vega= 1.872
0 0 28 F28 1 0 # 1 0    50 KPNO/IRIMKPRIME   +atm  lambda_c= 2.1648e+04  AB-Vega= 1.834
0 0 29 F29 1 0 # 1 0   112 ESO-NTT/SOFI_J.dat   +atm  lambda_c= 1.2359e+04  AB-Vega= 0.885
0 0 30 F30 1 0 # 1 0    67 ESO-NTT/SOFI_Js.dat   +atm  lambda_c= 1.2484e+04  AB-Vega= 0.909
0 0 31 F31 1 0 # 1 0    79 ESO-NTT/SOFI_H.dat   +atm  lambda_c= 1.6465e+04  AB-Vega= 1.358
0 0 32 F32 1 0 # 1 0    67 ESO-NTT/SOFI_Ks.dat   +atm  lambda_c= 2.1666e+04  AB-Vega= 1.837
0 0 33 F33 1 0 # 1 0   111 ESO/isaac_j.res  ESO_ETC+atm  lambda_c= 1.2356e+04  AB-Vega= 0.886
0 0 34 F34 1 0 # 1 0    94 ESO/isaac_js.res  ESO_ETC+atm  lambda_c= 1.2480e+04  AB-Vega= 0.909
0 0 35 F35 1 0 # 1 0    78 ESO/isaac_h.res  ESO_ETC+atm  lambda_c= 1.6496e+04  AB-Vega= 1.362
0 0 36 F36 1 0 # 1 0    66 ESO/isaac_ks.res  ESO_ETC+atm  lambda_c= 2.1667e+04  AB-Vega= 1.838
0 0 37 F37 1 0 # 1 0   900 ESO/WFI_u360specs.txt  2.2m-loiano+atm  lambda_c= 3.5279e+03  AB-Vega= 1.069
0 0 38 F38 1 0 # 1 0   761 ESO/u35_rebin.dat  2.2m-GOODS-MUSIC+atm  lambda_c= 3.5788e+03  AB-Vega= 0.974
0 0 39 F39 1 0 # 1 0   173 musyc/U_1030_tot.dat     lambda_c= 3.5843e+03  AB-Vega= 0.844
0 0 40 F40 1 0 # 1 0   141 musyc/U_1255_tot.dat     lambda_c= 3.7206e+03  AB-Vega= 0.586
0 0 41 F41 1 0 # 1 0   526 musyc/U_cdfs_tot.dat  alsoESO-DPS  lambda_c= 3.5006e+03  AB-Vega= 1.033
0 0 42 F42 1 0 # 1 0   141 musyc/U_hdfs_tot.dat     lambda_c= 3.7206e+03  AB-Vega= 0.586
0 0 43 F43 1 0 # 1 0   227 musyc/B_1030_tot.dat     lambda_c= 4.4144e+03  AB-Vega=-0.103
0 0 44 F44 1 0 # 1 0   236 musyc/B_1255_tot.dat     lambda_c= 4.4186e+03  AB-Vega=-0.102
0 0 45 F45 1 0 # 1 0   193 musyc/B_cdfs_tot.dat  alsoESO-DPS  lambda_c= 4.5910e+03  AB-Vega=-0.122
0 0 46 F46 1 0 # 1 0   227 musyc/B_hdfs_tot.dat     lambda_c= 4.4144e+03  AB-Vega=-0.103
0 0 47 F47 1 0 # 1 0   144 musyc/V_1030_tot.dat     lambda_c= 5.4175e+03  AB-Vega=-0.006
0 0 48 F48 1 0 # 1 0   139 musyc/V_1255_tot.dat     lambda_c= 5.4200e+03  AB-Vega=-0.006
0 0 49 F49 1 0 # 1 0    81 musyc/V_cdfs_tot.dat  alsoESO-DPS  lambda_c= 5.3724e+03  AB-Vega=-0.017
0 0 50 F50 1 0 # 1 0   144 musyc/V_hdfs_tot.dat     lambda_c= 5.4175e+03  AB-Vega=-0.006
0 0 51 F51 1 0 # 1 0   210 musyc/R_1030_tot.dat     lambda_c= 6.5618e+03  AB-Vega= 0.195
0 0 52 F52 1 0 # 1 0   210 musyc/R_1255_tot.dat     lambda_c= 6.5618e+03  AB-Vega= 0.195
0 0 53 F53 1 0 # 1 0   142 musyc/R_cdfs_tot.dat  alsoESO-DPS  lambda_c= 6.4992e+03  AB-Vega= 0.191
0 0 54 F54 1 0 # 1 0   210 musyc/R_hdfs_tot.dat     lambda_c= 6.5618e+03  AB-Vega= 0.195
0 0 55 F55 1 0 # 1 0    92 musyc/I_1030_tot.dat     lambda_c= 7.9768e+03  AB-Vega= 0.428
0 0 56 F56 1 0 # 1 0    93 musyc/I_1255_tot.dat     lambda_c= 7.9838e+03  AB-Vega= 0.428
0 0 57 F57 1 0 # 1 0   148 musyc/I_cdfs_tot.dat  alsoESO-DPS  lambda_c= 8.6372e+03  AB-Vega= 0.488
0 0 58 F58 1 0 # 1 0    98 musyc/I_hdfs_tot.dat     lambda_c= 8.0518e+03  AB-Vega= 0.436
0 0 59 F59 1 0 # 1 0    98 musyc/z_1030_tot.dat     lambda_c= 9.0404e+03  AB-Vega= 0.516
0 0 60 F60 1 0 # 1 0    98 musyc/z_1255_tot.dat     lambda_c= 9.0404e+03  AB-Vega= 0.516
0 0 61 F61 1 0 # 1 0    98 musyc/z_cdfs_tot.dat     lambda_c= 9.0404e+03  AB-Vega= 0.516
0 0 62 F62 1 0 # 1 0    98 musyc/z_hdfs_tot.dat     lambda_c= 9.0404e+03  AB-Vega= 0.516
0 0 63 F63 1 0 # 1 0    46 musyc/J_tot.dat  hdfs1-1030  lambda_c= 1.2452e+04  AB-Vega= 0.901
0 0 64 F64 1 0 # 1 0    74 musyc/H_tot.dat  hdfs1-1030  lambda_c= 1.6283e+04  AB-Vega= 1.341
0 0 65 F65 1 0 # 1 0   272 musyc/K_tot.dat  hdfs1-1030  lambda_c= 2.1307e+04  AB-Vega= 1.803
0 0 66 F66 1 0 # 1 0    85 musyc/Ja_tot.dat.2cols  hdfs2-1255  lambda_c= 1.2461e+04  AB-Vega= 0.905
0 0 67 F67 1 0 # 1 0    80 musyc/Ha_tot.dat.2cols  hdfs2-1255  lambda_c= 1.6346e+04  AB-Vega= 1.349
0 0 68 F68 1 0 # 1 0    62 musyc/Ka_tot.dat.2cols  hdfs2-1255  lambda_c= 2.1517e+04  AB-Vega= 1.821
0 0 69 F69 1 0 # 1 0    76 musyc/o3_hdfs_tot.dat     lambda_c= 4.9977e+03  AB-Vega=-0.092
0 0 70 F70 1 0 # 1 0   204 COSMOS/CFHT_filter_i.txt     lambda_c= 7.6831e+03  AB-Vega= 0.382
0 0 71 F71 1 0 # 1 0   269 COSMOS/CFHT_filter_u.txt     lambda_c= 3.8379e+03  AB-Vega= 0.330
0 0 72 F72 1 0 # 1 0    47 COSMOS/SDSS_filter_u.txt     lambda_c= 3.5565e+03  AB-Vega= 0.938
0 0 73 F73 1 0 # 1 0    89 COSMOS/SDSS_filter_g.txt     lambda_c= 4.7025e+03  AB-Vega=-0.104
0 0 74 F74 1 0 # 1 0    75 COSMOS/SDSS_filter_r.txt     lambda_c= 6.1766e+03  AB-Vega= 0.140
0 0 75 F75 1 0 # 1 0   100 COSMOS/SDSS_filter_i.txt     lambda_c= 7.4961e+03  AB-Vega= 0.353
0 0 76 F76 1 0 # 1 0   134 COSMOS/SDSS_filter_z.txt     lambda_c= 8.9467e+03  AB-Vega= 0.513
0 0 77 F77 1 0 # 1 0   161 COSMOS/SUBARU_filter_B.txt     lambda_c= 4.4480e+03  AB-Vega=-0.112
0 0 78 F78 1 0 # 1 0   107 COSMOS/SUBARU_filter_V.txt     lambda_c= 5.4702e+03  AB-Vega=-0.000
0 0 79 F79 1 0 # 1 0   118 COSMOS/SUBARU_filter_g.txt     lambda_c= 4.7609e+03  AB-Vega=-0.101
0 0 80 F80 1 0 # 1 0    92 COSMOS/SUBARU_filter_r.txt     lambda_c= 6.2755e+03  AB-Vega= 0.154
0 0 81 F81 1 0 # 1 0    94 COSMOS/SUBARU_filter_i.txt     lambda_c= 7.6712e+03  AB-Vega= 0.380
0 0 82 F82 1 0 # 1 0    81 COSMOS/SUBARU_filter_z.txt     lambda_c= 9.0282e+03  AB-Vega= 0.514
0 0 83 F83 1 0 # 1 0    14 COSMOS/SUBARU_filter_NB816.txt     lambda_c= 8.1509e+03  AB-Vega= 0.461
0 0 84 F84 1 0 # 1 0   142 KPNO/FLAMINGOS.BARR.J.MAN240.ColdWitness.txt   +atm  lambda_c= 1.2461e+04  AB-Vega= 0.906
0 0 85 F85 1 0 # 1 0    74 KPNO/FLAMINGOS.BARR.H.MAN109.ColdWitness.txt   +atm  lambda_c= 1.6339e+04  AB-Vega= 1.348
0 0 86 F86 1 0 # 1 0   110 KPNO/FLAMINGOS.BARR.Ks.MAN306A.ColdWitness.txt  COSMOS+atm  lambda_c= 2.1542e+04  AB-Vega= 1.825
0 0 87 F87 1 0 # 1 0   132 megaprime/cfht_mega_u_cfh9301.dat  CFHT-LS+atm  lambda_c= 3.8280e+03  AB-Vega= 0.325
0 0 88 F88 1 0 # 1 0   241 megaprime/cfht_mega_g_cfh9401.dat  CFHT-LS+atm  lambda_c= 4.8699e+03  AB-Vega=-0.090
0 0 89 F89 1 0 # 1 0   230 megaprime/cfht_mega_r_cfh9601.dat  CFHT-LS+atm  lambda_c= 6.2448e+03  AB-Vega= 0.151
0 0 90 F90 1 0 # 1 0   139 megaprime/cfht_mega_i_cfh9701.dat  CFHT-LS+atm  lambda_c= 7.6756e+03  AB-Vega= 0.382
0 0 91 F91 1 0 # 1 0   152 megaprime/cfht_mega_z_cfh9801.dat  CFHT-LS+atm  lambda_c= 8.8719e+03  AB-Vega= 0.510
0 0 92 F92 1 0 # 1 0   126 ESO/fors1_u_bess.res  ms1054+atm  lambda_c= 3.6825e+03  AB-Vega= 0.766
0 0 93 F93 1 0 # 1 0   219 ESO/fors1_b_bess.res  ms1054+atm  lambda_c= 4.3430e+03  AB-Vega=-0.107
0 0 94 F94 1 0 # 1 0   153 ESO/fors1_v_bess.res  ms1054+atm  lambda_c= 5.5226e+03  AB-Vega= 0.011
0 0 95 F95 1 0 # 1 0   199 ESO/fors1_r_bess.res  ESO_ETC+atm  lambda_c= 6.5303e+03  AB-Vega= 0.191
0 0 96 F96 1 0 # 1 0    87 ESO/fors1_i_bess.res  ESO_ETC+atm  lambda_c= 7.8749e+03  AB-Vega= 0.413
0 0 97 F97 1 0 # 1 0   178 ESO/fors1_u_gunn.res  ESO_ETC+atm  lambda_c= 3.6196e+03  AB-Vega= 0.972
0 0 98 F98 1 0 # 1 0    76 ESO/fors1_g_gunn.res  ESO_ETC+atm  lambda_c= 5.0867e+03  AB-Vega=-0.053
0 0 99 F99 1 0 # 1 0   157 ESO/fors1_v_gunn.res  ESO_ETC+atm  lambda_c= 4.0022e+03  AB-Vega= 0.048
0 0 100 F100 1 0 # 1 0   102 ESO/fors1_r_gunn.res  ESO_ETC+atm  lambda_c= 6.5160e+03  AB-Vega= 0.205
0 0 101 F101 1 0 # 1 0   125 ESO/fors1_z_gunn.res  ESO_ETC+atm  lambda_c= 8.9782e+03  AB-Vega= 0.513
0 0 102 F102 1 0 # 1 0   102 ESO/vimos_u.res  ESO_ETC+atm  lambda_c= 3.7495e+03  AB-Vega= 0.471
0 0 103 F103 1 0 # 1 0   147 ESO/wfi_BB_B123_ESO878.res  ESO_ETC+atm  lambda_c= 4.5922e+03  AB-Vega=-0.100
0 0 104 F104 1 0 # 1 0    80 ESO/wfi_BB_U50_ESO877.res  ESO_ETC_u35+atm  lambda_c= 3.6065e+03  AB-Vega= 0.935
0 0 105 F105 1 0 # 1 0    95 ESO/wfi_U50_ESO877_exend3200.res  ESO_ETC_u35+atm  lambda_c= 3.6023e+03  AB-Vega= 0.941
0 0 106 F106 1 0 # 1 0    80 ESO/wfi_BB_U38_ESO841.res  ESO_ETC_u38_narrower_than_COMBO17/C17+atm  lambda_c= 3.6865e+03  AB-Vega= 0.768
0 0 107 F107 1 0 # 1 0    80 COMBO-17.old/epsi_U.dat  U38_from_C17_website+atm  lambda_c= 3.6616e+03  AB-Vega= 0.817
0 0 108 F108 1 0 # 1 0   175 NOAO/steidel_Un_k1041bp_aug04.txt_ccd  kpno-mosaic+atm  lambda_c= 3.5765e+03  AB-Vega= 0.949
0 0 109 F109 1 0 # 1 0    87 NOAO/steidel_G_k1042bp_aug04.txt_ccd  kpno-mosaic+atm  lambda_c= 4.8077e+03  AB-Vega=-0.106
0 0 110 F110 1 0 # 1 0    91 NOAO/steidel_Rs_k1043bp_aug04.txt_ccd  kpno-mosaic+atm  lambda_c= 6.9408e+03  AB-Vega= 0.278
0 0 111 F111 1 0 # 1 0    70 COSMOS/gabasch_H_cosmos.txt  Gabasch-H-COSMOS+atm  lambda_c= 1.6427e+04  AB-Vega= 1.357
0 0 112 F112 1 0 # 1 0   116 CAPAK_v2/u_megaprime_sagem.res  cosmos-u  lambda_c= 3.8172e+03  AB-Vega= 0.322
0 0 113 F113 1 0 # 1 0   161 CAPAK_v2/B_subaru.res  cosmos-b  lambda_c= 4.4480e+03  AB-Vega=-0.112
0 0 114 F114 1 0 # 1 0   107 CAPAK_v2/V_subaru.res  cosmos-v  lambda_c= 5.4702e+03  AB-Vega=-0.000
0 0 115 F115 1 0 # 1 0    92 CAPAK_v2/r_subaru.res  cosmos-r  lambda_c= 6.2755e+03  AB-Vega= 0.154
0 0 116 F116 1 0 # 1 0    94 CAPAK_v2/i_subaru.res  cosmos-i  lambda_c= 7.6712e+03  AB-Vega= 0.380
0 0 117 F117 1 0 # 1 0    81 CAPAK_v2/z_subaru.res  cosmos-z  lambda_c= 9.0282e+03  AB-Vega= 0.514
0 0 118 F118 1 0 # 1 0    73 CAPAK_v2/flamingos_Ks.res  cosmos-k  lambda_c= 2.1519e+04  AB-Vega= 1.823
0 0 119 F119 1 0 # 1 0    18 CAPAK/galex1500.res  FUV  lambda_c= 1.5364e+03  AB-Vega= 2.128
0 0 120 F120 1 0 # 1 0    27 CAPAK/galex2500.res  NUV  lambda_c= 2.2992e+03  AB-Vega= 1.665
0 0 121 F121 1 0 # 1 0   164 UKIDSS/B_qe.txt  UDS  lambda_c= 4.4078e+03  AB-Vega=-0.099
0 0 122 F122 1 0 # 1 0    93 UKIDSS/R_qe.txt  UDS  lambda_c= 6.5083e+03  AB-Vega= 0.200
0 0 123 F123 1 0 # 1 0    96 UKIDSS/i_qe.txt  UDS  lambda_c= 7.6555e+03  AB-Vega= 0.378
0 0 124 F124 1 0 # 1 0    81 UKIDSS/z_qe.txt  UDS  lambda_c= 9.0602e+03  AB-Vega= 0.513
0 0 125 F125 1 0 # 1 0    75 UKIDSS/J.txt  UDS  lambda_c= 1.2502e+04  AB-Vega= 0.912
0 0 126 F126 1 0 # 1 0    83 UKIDSS/K.txt  UDS  lambda_c= 2.2060e+04  AB-Vega= 1.868
0 0 127 F127 1 0 # 1 0    96 NEWFIRM/j1_atmos.dat  nmbs  lambda_c= 1.0460e+04  AB-Vega= 0.636
0 0 128 F128 1 0 # 1 0    79 NEWFIRM/j2_atmos.dat  nmbs  lambda_c= 1.1946e+04  AB-Vega= 0.827
0 0 129 F129 1 0 # 1 0    73 NEWFIRM/j3_atmos.dat  nmbs  lambda_c= 1.2778e+04  AB-Vega= 0.946
0 0 130 F130 1 0 # 1 0    70 NEWFIRM/h1_atmos.dat  nmbs  lambda_c= 1.5601e+04  AB-Vega= 1.281
0 0 131 F131 1 0 # 1 0    64 NEWFIRM/h2_atmos.dat  nmbs  lambda_c= 1.7064e+04  AB-Vega= 1.417
0 0 132 F132 1 0 # 1 0    66 NEWFIRM/k.dat  nmbs  lambda_c= 2.1635e+04  AB-Vega= 1.831
0 0 133 F133 1 0 # 1 0    65 NEWFIRM/k_atmos.dat  nmbs  lambda_c= 2.1684e+04  AB-Vega= 1.836
0 0 134 F134 1 0 # 1 0    26 REST_FRAME/Bessel_UX.dat  noCCD  lambda_c= 3.5860e+03  AB-Vega= 0.816
0 0 135 F135 1 0 # 1 0    23 REST_FRAME/Bessel_B.dat  noCCD  lambda_c= 4.3711e+03  AB-Vega=-0.107
0 0 136 F136 1 0 # 1 0    26 REST_FRAME/Bessel_V.dat  noCCD  lambda_c= 5.4776e+03  AB-Vega= 0.003
0 0 137 F137 1 0 # 1 0    25 REST_FRAME/Bessel_R.dat  noCCD  lambda_c= 6.4592e+03  AB-Vega= 0.173
0 0 138 F138 1 0 # 1 0    24 REST_FRAME/Bessel_I.dat  noCCD  lambda_c= 8.0201e+03  AB-Vega= 0.432
0 0 139 F139 1 0 # 1 0   117 REST_FRAME/Johnson-Cousins_U.dat     lambda_c= 3.6299e+03  AB-Vega= 0.781
0 0 140 F140 1 0 # 1 0   229 REST_FRAME/Johnson-Cousins_B.dat     lambda_c= 4.2936e+03  AB-Vega=-0.056
0 0 141 F141 1 0 # 1 0   172 REST_FRAME/Johnson-Cousins_V.dat     lambda_c= 5.4698e+03  AB-Vega= 0.000
0 0 142 F142 1 0 # 1 0   159 REST_FRAME/Johnson-Cousins_R.dat     lambda_c= 6.4712e+03  AB-Vega= 0.180
0 0 143 F143 1 0 # 1 0    76 REST_FRAME/Johnson-Cousins_I.dat     lambda_c= 7.8726e+03  AB-Vega= 0.414
0 0 144 F144 1 0 # 1 0    23 REST_FRAME/Gunn_u.dat  incl_atmos_mirrors_ccd  lambda_c= 3.5588e+03  AB-Vega= 0.927
0 0 145 F145 1 0 # 1 0    25 REST_FRAME/Gunn_g.dat  incl_atmos_mirrors_ccd  lambda_c= 4.6726e+03  AB-Vega=-0.103
0 0 146 F146 1 0 # 1 0    26 REST_FRAME/Gunn_r.dat  incl_atmos_mirrors_ccd  lambda_c= 6.1739e+03  AB-Vega= 0.137
0 0 147 F147 1 0 # 1 0    25 REST_FRAME/Gunn_i.dat  incl_atmos_mirrors_ccd  lambda_c= 7.4926e+03  AB-Vega= 0.353
0 0 148 F148 1 0 # 1 0    30 REST_FRAME/Gunn_z.dat  incl_atmos_mirrors_ccd  lambda_c= 8.8740e+03  AB-Vega= 0.511
0 0 149 F149 1 0 # 1 0    13 REST_FRAME/Johnson-Morgan_U.dat  1951ApJ...114..522  lambda_c= 3.4902e+03  AB-Vega= 0.987
0 0 150 F150 1 0 # 1 0    13 REST_FRAME/Johnson-Morgan_B.dat  1951ApJ...114..522  lambda_c= 4.3610e+03  AB-Vega=-0.100
0 0 151 F151 1 0 # 1 0    14 REST_FRAME/Johnson-Morgan_V.dat  1951ApJ...114..522  lambda_c= 5.4755e+03  AB-Vega= 0.016
0 0 152 F152 1 0 # 1 0    23 REST_FRAME/maiz-apellaniz_Johnson_U.res  2006AJ....131.1184M  lambda_c= 3.5900e+03  AB-Vega= 0.769
0 0 153 F153 1 0 # 1 0    41 REST_FRAME/maiz-apellaniz_Johnson_B.res  2006AJ....131.1184M  lambda_c= 4.3722e+03  AB-Vega=-0.106
0 0 154 F154 1 0 # 1 0    47 REST_FRAME/maiz-apellaniz_Johnson_V.res  2006AJ....131.1184M  lambda_c= 5.4794e+03  AB-Vega= 0.002
0 0 155 F155 1 0 # 1 0    47 SDSS/u.dat  DR7+atm  lambda_c= 3.5565e+03  AB-Vega= 0.938
0 0 156 F156 1 0 # 1 0    89 SDSS/g.dat  DR7+atm  lambda_c= 4.7025e+03  AB-Vega=-0.104
0 0 157 F157 1 0 # 1 0    75 SDSS/r.dat  DR7+atm  lambda_c= 6.1756e+03  AB-Vega= 0.140
0 0 158 F158 1 0 # 1 0   100 SDSS/i.dat  DR7+atm  lambda_c= 7.4900e+03  AB-Vega= 0.352
0 0 159 F159 1 0 # 1 0   134 SDSS/z.dat  DR7+atm  lambda_c= 8.9467e+03  AB-Vega= 0.513
0 0 160 F160 1 0 # 1 0    89 2MASS/J.res     lambda_c= 1.2358e+04  AB-Vega= 0.885
0 0 161 F161 1 0 # 1 0    58 2MASS/H.res     lambda_c= 1.6458e+04  AB-Vega= 1.362
0 0 162 F162 1 0 # 1 0    63 2MASS/K.res     lambda_c= 2.1603e+04  AB-Vega= 1.830
0 0 163 F163 1 0 # 1 0    57 COMBO17/C17_420.res     lambda_c= 4.1797e+03  AB-Vega=-0.173
0 0 164 F164 1 0 # 1 0    23 COMBO17/C17_464.res     lambda_c= 4.6173e+03  AB-Vega=-0.170
0 0 165 F165 1 0 # 1 0    39 COMBO17/C17_485.res     lambda_c= 4.8603e+03  AB-Vega=-0.057
0 0 166 F166 1 0 # 1 0    27 COMBO17/C17_518.res     lambda_c= 5.1885e+03  AB-Vega=-0.050
0 0 167 F167 1 0 # 1 0    34 COMBO17/C17_571.res     lambda_c= 5.7172e+03  AB-Vega= 0.046
0 0 168 F168 1 0 # 1 0    21 COMBO17/C17_604.res     lambda_c= 6.0450e+03  AB-Vega= 0.107
0 0 169 F169 1 0 # 1 0    24 COMBO17/C17_646.res     lambda_c= 6.4518e+03  AB-Vega= 0.217
0 0 170 F170 1 0 # 1 0    21 COMBO17/C17_696.res     lambda_c= 6.9595e+03  AB-Vega= 0.268
0 0 171 F171 1 0 # 1 0    27 COMBO17/C17_753.res     lambda_c= 7.5310e+03  AB-Vega= 0.363
0 0 172 F172 1 0 # 1 0    24 COMBO17/C17_815.res     lambda_c= 8.1578e+03  AB-Vega= 0.462
0 0 173 F173 1 0 # 1 0    27 COMBO17/C17_855.res     lambda_c= 8.5571e+03  AB-Vega= 0.538
0 0 174 F174 1 0 # 1 0    23 COMBO17/C17_915.res     lambda_c= 9.1409e+03  AB-Vega= 0.505
0 0 175 F175 1 0 # 1 0    80 COMBO17/C17_U.res     lambda_c= 3.6527e+03  AB-Vega= 0.836
0 0 176 F176 1 0 # 1 0    83 COMBO17/C17_B.res     lambda_c= 4.5726e+03  AB-Vega=-0.124
0 0 177 F177 1 0 # 1 0    80 COMBO17/C17_V.res     lambda_c= 5.3709e+03  AB-Vega=-0.017
0 0 178 F178 1 0 # 1 0   114 COMBO17/C17_R.res     lambda_c= 6.4664e+03  AB-Vega= 0.183
0 0 179 F179 1 0 # 1 0    99 COMBO17/C17_I.res     lambda_c= 8.5538e+03  AB-Vega= 0.483
0 0 180 F180 1 0 # 1 0    39 Subaru_MB/IA427.dat     lambda_c= 4.2600e+03  AB-Vega=-0.161
0 0 181 F181 1 0 # 1 0    42 Subaru_MB/IA445.dat     lambda_c= 4.4427e+03  AB-Vega=-0.141
0 0 182 F182 1 0 # 1 0    28 Subaru_MB/IA464.dat     lambda_c= 4.6333e+03  AB-Vega=-0.167
0 0 183 F183 1 0 # 1 0    32 Subaru_MB/IA484.dat     lambda_c= 4.8473e+03  AB-Vega=-0.037
0 0 184 F184 1 0 # 1 0    34 Subaru_MB/IA505.dat     lambda_c= 5.0608e+03  AB-Vega=-0.077
0 0 185 F185 1 0 # 1 0    35 Subaru_MB/IA527.dat     lambda_c= 5.2593e+03  AB-Vega=-0.035
0 0 186 F186 1 0 # 1 0    40 Subaru_MB/IA550.dat     lambda_c= 5.4950e+03  AB-Vega= 0.006
0 0 187 F187 1 0 # 1 0    42 Subaru_MB/IA574.dat     lambda_c= 5.7629e+03  AB-Vega= 0.054
0 0 188 F188 1 0 # 1 0    39 Subaru_MB/IA598.dat     lambda_c= 6.0071e+03  AB-Vega= 0.101
0 0 189 F189 1 0 # 1 0    33 Subaru_MB/IA624.dat     lambda_c= 6.2308e+03  AB-Vega= 0.142
0 0 190 F190 1 0 # 1 0    35 Subaru_MB/IA651.dat     lambda_c= 6.4984e+03  AB-Vega= 0.236
0 0 191 F191 1 0 # 1 0    39 Subaru_MB/IA679.dat     lambda_c= 6.7816e+03  AB-Vega= 0.245
0 0 192 F192 1 0 # 1 0    33 Subaru_MB/IA709.dat     lambda_c= 7.0735e+03  AB-Vega= 0.287
0 0 193 F193 1 0 # 1 0    32 Subaru_MB/IA738.dat     lambda_c= 7.3595e+03  AB-Vega= 0.334
0 0 194 F194 1 0 # 1 0    28 Subaru_MB/IA768.dat     lambda_c= 7.6804e+03  AB-Vega= 0.387
0 0 195 F195 1 0 # 1 0    32 Subaru_MB/IA797.dat     lambda_c= 7.9662e+03  AB-Vega= 0.432
0 0 196 F196 1 0 # 1 0    25 Subaru_MB/IA827.dat     lambda_c= 8.2468e+03  AB-Vega= 0.475
0 0 197 F197 1 0 # 1 0    58 Subaru_MB/IA856.dat     lambda_c= 8.5648e+03  AB-Vega= 0.534
0 0 198 F198 1 0 # 1 0    26 Subaru_MB/IA907.dat     lambda_c= 9.0704e+03  AB-Vega= 0.497
0 0 199 F199 1 0 # 1 0    61 COSMOS/CFHT_filter_Ks.txt     lambda_c= 2.1571e+04  AB-Vega= 1.827
0 0 200 F200 1 0 # 1 0   236 hst/wfc3/IR/f098m.dat  calcband_wfc3-ir-f098m  lambda_c= 9.8668e+03  AB-Vega= 0.558
0 0 201 F201 1 0 # 1 0   206 hst/wfc3/IR/f105w.dat     lambda_c= 1.0545e+04  AB-Vega= 0.641
0 0 202 F202 1 0 # 1 0    87 hst/wfc3/IR/f125w.dat     lambda_c= 1.2471e+04  AB-Vega= 0.895
0 0 203 F203 1 0 # 1 0   105 hst/wfc3/IR/f140w.dat     lambda_c= 1.3924e+04  AB-Vega= 1.072
0 0 204 F204 1 0 # 1 0   132 hst/wfc3/IR/f160w.dat     lambda_c= 1.5396e+04  AB-Vega= 1.250
0 0 205 F205 1 0 # 1 0   528 hst/wfc3/UVIS/f218w.dat  calcband_wfc3-uvis1-f218w  lambda_c= 2.2272e+03  AB-Vega= 1.690
0 0 206 F206 1 0 # 1 0  1132 hst/wfc3/UVIS/f225w.dat     lambda_c= 2.3707e+03  AB-Vega= 1.660
0 0 207 F207 1 0 # 1 0   346 hst/wfc3/UVIS/f275w.dat     lambda_c= 2.7086e+03  AB-Vega= 1.501
0 0 208 F208 1 0 # 1 0    77 hst/wfc3/UVIS/f336w.dat     lambda_c= 3.3537e+03  AB-Vega= 1.185
0 0 209 F209 1 0 # 1 0   130 hst/wfc3/UVIS/f390w.dat     lambda_c= 3.9219e+03  AB-Vega= 0.220
0 0 210 F210 1 0 # 1 0    91 hst/wfc3/UVIS/f438w.dat     lambda_c= 4.3256e+03  AB-Vega=-0.154
0 0 211 F211 1 0 # 1 0   114 hst/wfc3/UVIS/f475w.dat     lambda_c= 4.7715e+03  AB-Vega=-0.100
0 0 212 F212 1 0 # 1 0   206 hst/wfc3/UVIS/f555w.dat     lambda_c= 5.3086e+03  AB-Vega=-0.027
0 0 213 F213 1 0 # 1 0   172 hst/wfc3/UVIS/f606w.dat     lambda_c= 5.8925e+03  AB-Vega= 0.080
0 0 214 F214 1 0 # 1 0    90 hst/wfc3/UVIS/f625w.dat     lambda_c= 6.2451e+03  AB-Vega= 0.143
0 0 215 F215 1 0 # 1 0    74 hst/wfc3/UVIS/f775w.dat     lambda_c= 7.6576e+03  AB-Vega= 0.378
0 0 216 F216 1 0 # 1 0   113 hst/wfc3/UVIS/f814w.dat     lambda_c= 8.0595e+03  AB-Vega= 0.417
0 0 217 F217 1 0 # 1 0    74 REST_FRAME/UV1600.dat  Width_350A  lambda_c= 1.5967e+03  AB-Vega= 2.033
0 0 218 F218 1 0 # 1 0    72 REST_FRAME/UV2800.dat  Width_350A  lambda_c= 2.7942e+03  AB-Vega= 1.452
0 0 219 F219 1 0 # 1 0    54 WIRCam/cfh8101_J.txt   +atm  lambda_c= 1.2530e+04  AB-Vega= 0.914
0 0 220 F220 1 0 # 1 0    76 WIRCam/cfh8201_H.txt   +atm  lambda_c= 1.6294e+04  AB-Vega= 1.342
0 0 221 F221 1 0 # 1 0    58 WIRCam/cfh8302_Ks.txt   +atm  lambda_c= 2.1574e+04  AB-Vega= 1.827
0 0 222 F222 1 0 # 1 0   135 VISTA_Y  +atm  lambda_c= 1.0211e+04  AB-Vega= xxxx
0 0 223 F223 1 0 # 1 0   233 VISTA_J  +atm  lambda_c= 1.2541e+04  AB-Vega= xxxx
0 0 224 F224 1 0 # 1 0   382 VISTA_H  +atm  lambda_c= 1.6464e+04  AB-Vega= xxxx
0 0 225 F225 1 0 # 1 0   434 VISTA_Ks  +atm  lambda_c= 2.1487e+04  AB-Vega= xxxx
0 0 226 F226 1 0 # 1 2 'MIPS_24' (http://irsa.ipac.caltech.edu/data/SPITZER/docs/files/spitzer/MIPSfiltsumm.txt)
