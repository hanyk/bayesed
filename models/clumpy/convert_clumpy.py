#!/usr/bin/env python
import h5py
import sys
import os

if len(sys.argv)<2:
    print "./models/clumpy/convert_clumpy.py [tor|toragn]"
    sys.exit()
agn=sys.argv[1]
key="clumpy201410"+agn
path="models/"
h = h5py.File(path+'clumpy_models_201410_tvavg.hdf5')

output1=open(path+key+".par", 'w')
output1.write("# "+key+"\n")
output1.write("# http://www.pa.uky.edu/clumpy/models/clumpy_models_201410_tvavg.hdf5\n")
output1.write("# N0 Y i q sig tv\n")

output2=open(path+key+".spec", 'w')
output2.write("# lambda[micron]\n")
output2.write("# lambda*F_lambda/F_AGN\n")
wave=h['wave'][:]
output2.write("%g" % (wave[0]))
for i in range(1,len(wave)):
    output2.write(" %g" % (wave[i]))
output2.write("\n")

os.symlink(key+".par", path+key+"_derived.par")
output3=open(path+key+"_derived.spec", 'w')
output3.write("# "+key)
#output3.write(" f2 (dust covering fraction), ptype1 (the probability to see the AGN unobscured), s10 & s18 (the 10 & 18-micron silicate feature strengths), lam10 & lam18 (the wavelengths where the extrema of the 10 and 18-micron features occur)\n")
#output3.write("f2 ptype1 s10 s18 lam10 lam18\n")
#output3.write("0 0 0 0 0 0\n")
output3.write(" f2 (dust covering fraction), ptype1 (the probability to see the AGN unobscured)\n")
output3.write("f2 ptype1\n")
output3.write("0 0\n")

if agn=="tor": flux=h['flux_tor'][:]
if agn=="toragn": flux=h['flux_toragn'][:]

N0 = h['N0'][:]
Y = h['Y'][:]
i = h['i'][:]
q = h['q'][:]
sig = h['sig'][:]
tv = h['tv'][:]

f2 = h['f2'][:]
ptype1 = h['ptype1'][:]
s10 = h['s10'][:]
s18 = h['s18'][:]
lam10 = h['lam10'][:]
lam18 = h['lam18'][:]

for ix in range(len(flux)):
    output1.write("%g %g %g %g %g %g\n" % (N0[ix],Y[ix],i[ix],q[ix],sig[ix],tv[ix]))
    output2.write("%g" % (flux[ix][0]))
    for iy in range(1,len(wave)):
        output2.write(" %g" % (flux[ix][iy]))
    output2.write("\n")
    #output3.write("%g %g %g %g %g %g\n" % (f2[ix],ptype1[ix],s10[ix],s18[ix],lam10[ix],lam18[ix]))
    output3.write("%g %g\n" % (f2[ix],ptype1[ix]))

h.close()
